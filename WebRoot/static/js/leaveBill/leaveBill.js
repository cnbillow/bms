require(['vue','ELEMENT','base','vue-resource','Message'],function(Vue,ELEMENT,baseUtil,VueResource,messageHelper){
	Vue.use(VueResource);
	//安装element
	Vue.use(ELEMENT);
	//安装message
	Vue.use(messageHelper);
	var billList = new Vue({
		el:"#billList",
		data:{
			tableData:[],
			dialogVisible:false,
			leaveBill:{
				days:"",
				content:"",
				remark:""
			},
			geturl:baseUtil.WebRootUrl+"/LeaveBillControl/getLeaveBillList",
			addURL:baseUtil.WebRootUrl+"/LeaveBillControl/addLeaveBill",
			deleteURL:baseUtil.WebRootUrl+"/LeaveBillControl//deleteLeaveBill",
			applyURL:baseUtil.WebRootUrl+"/LeaveBillFlowControl/startProcess",
			viewImg:baseUtil.WebRootUrl+"/LeaveBillFlowControl/viewImg",
			rules:{
				days:[{type: 'number', message: '必须为数字值',trigger: 'blur'}],
				content:[{required:true,message:"请输入原因",trigger: 'blur'}]
			}
		},
		mounted:function(){
    		this.getData();
    	},
    	methods:{
    		getData(){
    			var vm = this;
    			vm.$http.get(vm.geturl)
    			.then((response) =>{
    				if(response.data.result == "200"){
    					vm.tableData = response.data.dataList;
    				}else{
		    			 console.log(response);
		    		 }
    			})
    			.catch((response) => {
    				console.log(response);
    			})
    		},
    		showaddLeaveBill(){
    			this.dialogVisible = true;
    		},
    		addLeaveBill(form){
    			var vm = this;
    			vm.$refs[form].validate((valid) =>{
    				if(valid){
    					vm.$http.post(vm.addURL,vm.leaveBill,{emulateJSON:true})
    					.then((response)=>{
    						if(response.data.result == "200"){
    							vm.$showMess({message:'添加成功!',messType:'success'});
    						}else{
    							vm.$showMess({message:'添加失败!',messType:'error'});
    						}
    						vm.dialogVisible = false;
        					vm.$refs[form].resetFields();
        					vm.getData();
    					})
    					.catch((response) => {
    						vm.$showMess({message:'添加失败!',messType:'error'});
    					})
    					
    				}
    			});
    			
    		},
    		deleteBill(row){
    			var id = row.id;
    			var vm = this;
    			vm.$http.post(vm.deleteURL,{"id":id},{emulateJSON:true})
    			.then((response) => {
    				if(response.data.result == "200"){
						vm.$showMess({message:'删除成功!',messType:'success'});
					}else{
						vm.$showMess({message:'删除失败!',messType:'error'});
					}
    				vm.getData();
    			})
    			.catch((response) => {
    				vm.$showMess({message:'删除失败!',messType:'error'});
    			})
    		},
    		applyBill(row){
    			var id = row.id;
    			var vm = this;
    			vm.$http.post(vm.applyURL,{"id":id},{emulateJSON:true})
    			.then((response) =>{
    				if(response.data.result == "200"){
						vm.$showMess({message:'申请成功!',messType:'success'});
					}else{
						vm.$showMess({message:'申请失败!',messType:'error'});
					}
    				vm.getData();
    			})
    			.catch((response) =>{
    				vm.$showMess({message:'申请失败!',messType:'error'});
    			})
    		},
    		viewBill(row){
    			var key = row.businesskey;
    			var vm = this;
    			 window.open(vm.viewImg+"?businesskey="+key);
    		},
    		dateFormat(row,column){
    			var date = row[column.property];
				if(date == null){
					return "";
				}else{
					return new Date(date).format("yyyy-MM-DD HH:mm:ss");
				}
		   }
    	}
	})
})