package pers.yaoliguo.bms.entity;

import java.io.Serializable;
import java.util.Date;

public class leaveBill implements Serializable{
	
    private String id;

    private Integer days;

    private String content;

    private Date leveDate;

    private String remark;

    private String userId;
    
    private Integer state;
    
    private String businesskey;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getLeveDate() {
        return leveDate;
    }

    public void setLeveDate(Date leveDate) {
        this.leveDate = leveDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getBusinesskey() {
		return businesskey;
	}

	public void setBusinesskey(String businesskey) {
		this.businesskey = businesskey;
	}
}