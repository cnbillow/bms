package pers.yaoliguo.bms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pers.yaoliguo.bms.dao.SysRoleMenuDao;
import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysRoleMenuKey;
import pers.yaoliguo.bms.service.ISysMenuRoleService;

/**
 * 
 * @ClassName: SysMenuRoleService 
 * @Description: TODO
 * @author: wangyi
 * @date: 2017年7月20日 下午2:08:03
 */
@Service("sysMenuRoleService")
@Transactional
public class SysMenuRoleService implements ISysMenuRoleService{
    @Autowired 
    SysRoleMenuDao sysRoleMenuDao;
	@Override
	public List<SysRoleMenuKey> getrolemenus(SysRoleMenuKey record) {
		
		return sysRoleMenuDao.getrolemenus(record);
	}
	@Override
	public int deleteByPrimaryKey(SysRoleMenuKey key) {
		
		return sysRoleMenuDao.deleteByPrimaryKey(key);
	}
	@Override
	public int insert(SysRoleMenuKey record) {
		
		return sysRoleMenuDao.insert(record);
	}
	@Override
	public int deleteByRoleId(SysRoleMenuKey key) {
		
		return sysRoleMenuDao.deleteByRoleId(key);
	}
}
