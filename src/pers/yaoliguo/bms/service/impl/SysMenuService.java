package pers.yaoliguo.bms.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pers.yaoliguo.bms.dao.SysMenuDao;
import pers.yaoliguo.bms.entity.SysMenu;
import pers.yaoliguo.bms.entity.SysRole;
import pers.yaoliguo.bms.entity.SysRoleMenuKey;
import pers.yaoliguo.bms.service.ISysMenuService;
import pers.yaoliguo.bms.uitl.MapUtils;

/**
 * @ClassName:       SysMenuService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年6月30日        下午8:56:50
 */
@Service("sysMenuService")
public class SysMenuService implements ISysMenuService {
	
	@Autowired
	SysMenuDao sysMenuDao;

	@Override
	public int deleteByPrimaryKey(String id) {
		
		return sysMenuDao.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SysMenu record) {
		
		return sysMenuDao.insert(record);
	}

	@Override
	public int insertSelective(SysMenu record) {
		
		return sysMenuDao.insertSelective(record);
	}

	@Override
	public SysMenu selectByPrimaryKey(String id) {
		
		return sysMenuDao.selectByPrimaryKey(id);
	}

	@Override
	public int updateByPrimaryKeySelective(SysMenu record) {
		
		return sysMenuDao.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SysMenu record) {
		
		return sysMenuDao.updateByPrimaryKey(record);
	}

	@Override
	public int selectCount(SysMenu record) {
		Map map = MapUtils.returnMap(record);
		return sysMenuDao.selectCount(map);
	}

	@Override
	public List<SysMenu> selectAll(SysMenu record) {
		Map map = MapUtils.returnMap(record);
		return sysMenuDao.selectAll(map);
	}

	@Override
	public int removeChildren(SysMenu record) {
		
		List<SysMenu> list = selectChildren(record);
		for (SysMenu sysMenu : list) {
			sysMenu.setDel(true);
			sysMenuDao.updateByPrimaryKeySelective(sysMenu);
		}
		
		return list.size();
	}
	
	public List<SysMenu> selectChildren(SysMenu record){
		SysMenu m = new SysMenu();
		m.setPid(record.getId());
		m.setDel(false);
		
		List<SysMenu> list = selectAll(m);
		List<SysMenu> list1 = null;
		List<SysMenu> listCount = new ArrayList<SysMenu>(); 
		if(list.size() > 0){
			listCount.addAll(list);
			for (SysMenu sysMenu : list) {
				list1 = selectChildren(sysMenu);
				if(list1.size() > 0)
				{
					listCount.addAll(list1);
				}
			}
		}
		
		return listCount;
	}

	@Override
	public List<SysMenu> selectByIds(List<SysRoleMenuKey> list) {
		
		return sysMenuDao.selectByIds(list);
	}

}
