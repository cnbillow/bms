package pers.yaoliguo.bms.service;

import java.util.List;
import java.util.Map;

import pers.yaoliguo.bms.common.PagerBean;
import pers.yaoliguo.bms.entity.leaveBill;

/**
 * @ClassName:       IleaveBillService
 * @Description:    TODO
 * @author:            yao
 * @date:            2017年8月16日        下午9:21:57
 */
public interface ILeaveBillService {
	
	 int deleteByPrimaryKey(String id);

    int insert(leaveBill record);

    int insertSelective(leaveBill record);

    leaveBill selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(leaveBill record);

    int updateByPrimaryKey(leaveBill record);
    
    List<leaveBill> selectAll(leaveBill record, PagerBean page);
}
